#!/bin/bash
#Ardo Erik
#Skript loob etteantud aadressile veebilehe(local).

#Väljumiskoodid
#1 - vale arv param
#2 - on vaja juure õigusi
#3 - nimelahendus juba olemas
#4 - ebakorrektne URL
#88 - URL juba loodud



#Juurkasutaja õiguste olemasolu kontroll
if [ $UID -ne 0 ]
then
	echo "on vaja juurkasutaja õigusi" 
	exit 2
fi

#parameetrite arvu kontroll JA url muutuja väärtustamine
if [ $# -ne  1 ] 
	then 
	 echo "kasutamine: $0 'valitud url' "
	 exit 1
	else
	 URL=$1

fi

#kontrollib apache olemasolu ja vajadusel paigaldab selle
type apache2 > /dev/null 2>&1

if [ $? -ne 0 ]
then
	echo "pole apachet installitud, installin nüüd" 
	apt-get update > /dev/null 2>&1 && apt-get install apache2 -y || exit 1
fi


# kontroll, kas lahendus on olemas
if [ -d /var/www/$URL ]
 then
	echo "lahendus juba olemas"
	exit 88
fi



#hosts faili localhost aadressile lisatakse uue lehe kirje
echo "127.0.0.1	"$URL >> /etc/hosts


#uue apache conf faili loomine konkreetse URLi jaoks

cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/$URL.conf

ERRLOG='/var/log/apache2/'$URL'.error.log'
CUSTOMLOG='/var/log/apache2/'$URL'.access.log'
ROOT='/var/www/'$URL

#HTML lehe importimine
mkdir $ROOT
cp /var/www/html/index.html $ROOT'/index.html'

#URL lehe faili muutmine 
LISAND='<h1>Oled lehel: '$URL'<\/h1>'

sed -i "/<body>/a\\ $LISAND"  $ROOT'/index.html'

#apache confimine
sed -i '/ErrorLog/c\ErrorLog '$ERRLOG /etc/apache2/sites-available/$URL.conf

sed -i '/CustomLog/c\CustomLog '$CUSTOMLOG' combined' /etc/apache2/sites-available/$URL.conf

sed  -i 's/#ServerName.*/ServerName '$URL'/g' /etc/apache2/sites-available/$URL.conf

sed -i '/DocumentRoot/c\DocumentRoot '$ROOT /etc/apache2/sites-available/$URL.conf


a2ensite $URL.conf > /dev/null 2>&1

service apache2 reload

echo $URL" loodud. Naudi!"




